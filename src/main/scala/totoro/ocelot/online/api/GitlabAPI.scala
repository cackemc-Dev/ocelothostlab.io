package totoro.ocelot.online.api

import akka.actor.ClassicActorSystemProvider
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpRequest, HttpResponse, StatusCodes, headers}
import akka.http.scaladsl.unmarshalling.Unmarshal
import spray.json.enrichString
import totoro.ocelot.online.{Ocelot, Settings}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

object GitlabAPI {
  private val OcelotProjId = 9941848

  var latestDevelopJob: GitlabJob = _
  var latestRelease: GitlabRelease = _

  def syncReleaseBuildInfo()(implicit system: ClassicActorSystemProvider, executor: ExecutionContext): Unit = {
    Http().singleRequest(
      HttpRequest(uri = s"https://gitlab.com/api/v4/projects/$OcelotProjId/releases/")
        .withHeaders(headers.RawHeader("PRIVATE-TOKEN", Settings.get.desktopGitlabToken))
    ).flatMap {
      case HttpResponse(status, _, entity, _) =>
        Future.successful(status) zip Unmarshal(entity).to[String]
      case _ =>
        Future.failed(new RuntimeException("Cannot synchronize Ocelot Desktop release build info!"))
    }.onComplete {
      case Success(response) => response match {
        case (StatusCodes.OK, text) =>
          latestRelease = text.parseJson.convertTo[List[GitlabRelease]].head
        case (status, text) =>
          Ocelot.log.error(
            s"Got error from GitLab while synchronizing Ocelot Desktop release build info!\n" +
              s"$status: $text"
          )
      }
      case Failure(exception) =>
        Ocelot.log.error(exception)
    }
  }

  def syncDevelopmentBuildInfo()(implicit system: ClassicActorSystemProvider, executor: ExecutionContext): Unit = {
    Http().singleRequest(
      HttpRequest(uri = s"https://gitlab.com/api/v4/projects/$OcelotProjId/jobs/")
        .withHeaders(headers.RawHeader("PRIVATE-TOKEN", Settings.get.desktopGitlabToken))
    ).flatMap {
      case HttpResponse(status, _, entity, _) =>
        Future.successful(status) zip Unmarshal(entity).to[String]
      case _ =>
        Future.failed(new RuntimeException("Cannot synchronize Ocelot Desktop dev build info!"))
    }.onComplete {
      case Success(response) => response match {
        case (StatusCodes.OK, text) =>
          latestDevelopJob = text.parseJson.convertTo[List[GitlabJob]].head
          Ocelot.log.info("Received info about new Ocelot Desktop build!\n" +
            s"(${latestDevelopJob.commit.short_id} by ${latestDevelopJob.commit.author_name} at ${latestDevelopJob.commit.authored_date})")
        case (status, text) =>
          Ocelot.log.error(
            s"Got error from GitLab while synchronizing Ocelot Desktop dev build info!\n" +
            s"$status: $text"
          )
      }
      case Failure(exception) =>
        Ocelot.log.error(exception)
    }
  }
}
