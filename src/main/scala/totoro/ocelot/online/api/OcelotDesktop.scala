package totoro.ocelot.online.api

import spray.json.{DefaultJsonProtocol, RootJsonFormat}

object OcelotDesktop extends DefaultJsonProtocol {
  case class Dev(id: String, date: String)
  case class Release(version: String, date: String)
  case class Info(dev: Dev, release: Release)

  def collectInfo(dev: GitlabJob, release: GitlabRelease): Info =
    Info(
      Dev(dev.commit.id, dev.commit.authored_date),
      Release(release.tag_name, release.released_at)
    )

  implicit val devFormat: RootJsonFormat[Dev] = jsonFormat2(Dev)
  implicit val releaseFormat: RootJsonFormat[Release] = jsonFormat2(Release)
  implicit val infoFormat: RootJsonFormat[Info] = jsonFormat2(Info)
}
